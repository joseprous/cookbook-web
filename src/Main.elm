module Main exposing (..)

import Element exposing (Element, el, column, row, fill, spacing, padding, alignRight, alignLeft, width, px, rgb)
import Element.Events
import Element.Input as Input
import Element.Font as Font
import Element.Border as Border
import Element.Region as Region
import Browser
import Browser.Navigation as Nav
import Json.Encode as Encode exposing (object, Value)
import Json.Decode exposing (Decoder, field, int, float, string, list, map2, map3, map4, decodeString)
import List exposing (map)
import Url
import Http exposing (..)
import Route exposing (Route, fromUrl)
import I18Next exposing (t, tr, Translations, Delims(..), translationsDecoder, initialTranslations)
import Html.Attributes as Attributes

type alias Recipe =
    { id : Int
    , name : String
    , text : String
    }

type alias NewRecipe =
    { name : String
    , text : String
    }

type alias Recipes = List Recipe

type Status = Loading | Loaded | LoadFailed String

type Page = PNone | PRecipes Recipes | PRecipe Recipe | PNewRecipe

type alias Dialog =
    { name : String
    , id : Int
    }

type alias Model =
  { key : Nav.Key
  , url : Url.Url
  , page : Page
  , status : Status
  , apiUrl : String
  , translations : Translations
  , language : Lang
  , name : String
  , text : String
  , dialog : Maybe Dialog
  }

type Lang = EN | ES

type Msg
  = LinkClicked Browser.UrlRequest
  | UrlChanged Url.Url
  | GotRecipes (Result Http.Error Recipes)
  | GotRecipe (Result Http.Error Recipe)
  | GotTranslations (Result Http.Error Translations)
  | ChangeLang Lang
  | NameChanged String
  | TextChanged String
  | OkNewRecipe
  | CancelNewRecipe
  | GotNewRecipe (Result Http.Error Int)
  | DeleteRecipe Int
  | RecipeDeleted (Result Http.Error String)
  | ShowDialog String Int
  | CancelDialog
  | OkDialog Int
  | NoAction

main : Program String Model Msg
main =
  Browser.application
    { init = init
    , view = view
    , update = update
    , subscriptions = subscriptions
    , onUrlChange = UrlChanged
    , onUrlRequest = LinkClicked
    }

init : String -> Url.Url -> Nav.Key -> ( Model, Cmd Msg )
init apiUrl url key =
    ({ key = key
     , url = url
     , page = PNone
     , status = Loading
     , apiUrl = apiUrl
     , translations = initialTranslations
     , language = ES
     , name = ""
     , text = ""
     , dialog = Nothing
     }
    , getTranslation ES
    )

changeRouteTo : Model -> String -> Maybe Route -> ( Model, Cmd Msg)
changeRouteTo model apiUrl maybeRoute =
    case maybeRoute of
        Nothing -> (model, Cmd.none)
        Just Route.Home -> (model, getRecipes apiUrl)
        Just (Route.Recipe recipeId) -> (model, getRecipe apiUrl recipeId)
        Just Route.NewRecipe -> ({ model | page = PNewRecipe, status = Loaded}, Cmd.none)

getTranslation : Lang -> Cmd Msg
getTranslation lang =
    let langUrl = case lang of
                      EN -> "translations/en.json"
                      ES -> "translations/es.json"
    in Http.get
        { url = langUrl
        , expect = Http.expectJson GotTranslations translationsDecoder
        }

getRecipes : String -> Cmd Msg
getRecipes apiUrl =
    Http.get
        { url = apiUrl ++ "recipes"
        , expect = Http.expectJson GotRecipes recipesDecoder
        }

getRecipe : String -> Int -> Cmd Msg
getRecipe apiUrl recipeId =
    Http.get
        { url = apiUrl ++ "recipes/" ++ String.fromInt recipeId
        , expect = Http.expectJson GotRecipe recipeDecoder
        }

encodeNewRecipe : NewRecipe -> Value
encodeNewRecipe recipe =
    object [ ("name", Encode.string recipe.name)
           , ("text", Encode.string recipe.text)
           ]

postRecipe : String -> NewRecipe -> Cmd Msg
postRecipe apiUrl recipe =
    Http.post
        { url = apiUrl ++ "recipes"
        , body = encodeNewRecipe recipe |> Http.jsonBody
        , expect = Http.expectJson GotNewRecipe idDecoder
        }

deleteRecipe : String -> Int -> Cmd Msg
deleteRecipe apiUrl recipeId =
    Http.request
        { method = "DELETE"
        , headers = []
        , url = apiUrl ++ "recipes/" ++ String.fromInt recipeId
        , body = Http.emptyBody
        , expect = Http.expectString RecipeDeleted
        , timeout = Nothing
        , tracker = Nothing
        }

recipesDecoder : Decoder Recipes
recipesDecoder = list recipeDecoder

idDecoder : Decoder Int
idDecoder = field "id" int

recipeDecoder : Decoder Recipe
recipeDecoder =
    map3 Recipe
        (field "id" int)
        (field "name" string)
        (field "text" string)

subscriptions : Model -> Sub Msg
subscriptions _ =
  Sub.none

errorToString : Error -> String
errorToString err =
    case err of
        BadUrl s -> "BadUrl " ++ s
        Timeout -> "Timeout"
        NetworkError -> "NetworkError"
        BadStatus _ -> "BadStatus"
        BadBody s -> "BadBody " ++ s

update : Msg -> Model -> ( Model, Cmd Msg )
update msg model =
  case msg of
    LinkClicked urlRequest ->
      case urlRequest of
        Browser.Internal url ->
          ( model, Nav.pushUrl model.key (Url.toString url) )
        Browser.External href ->
          ( model, Nav.load href )
    UrlChanged url ->
        changeRouteTo { model | url = url } model.apiUrl (Route.fromUrl url)
    GotRecipes result ->
        case result of
            Ok recipes -> ({ model | page = PRecipes recipes, status = Loaded }, Cmd.none)
            Err _ -> ({ model | status = LoadFailed "Recipes"}, Cmd.none)
    GotRecipe result ->
        case result of
            Ok recipe -> ({ model | page = PRecipe recipe, status = Loaded }, Cmd.none)
            Err err -> ({ model | status = LoadFailed "Recipe"}, Cmd.none)
    GotTranslations result ->
        case result of
            Ok translations ->
                if model.status == Loading then
                    changeRouteTo { model | translations = translations } model.apiUrl (Route.fromUrl model.url)
                else
                    ( { model | translations = translations } , Cmd.none )
            Err err -> ({ model | status = LoadFailed ("Translations " ++ errorToString err)}, Cmd.none)
    ChangeLang lang -> ({ model | language = lang}, getTranslation lang)
    NameChanged s -> ({ model | name = s }, Cmd.none)
    TextChanged s -> ({ model | text = s }, Cmd.none)
    OkNewRecipe -> (model, postRecipe model.apiUrl { name = model.name, text = model.text })
    CancelNewRecipe ->
        let url = String.slice 0 (String.length "#/newrecipe/" * (-1))(Url.toString model.url)
        in ({ model | name = "", text = "" }, Nav.pushUrl model.key url )
    GotNewRecipe id ->
        let url = String.slice 0 (String.length "#/newrecipe/" * (-1))(Url.toString model.url)
        in ({ model | name = "", text = "" }, Nav.pushUrl model.key url )
    DeleteRecipe id -> (model, deleteRecipe model.apiUrl id)
    RecipeDeleted result ->
        case result of
            Ok _ -> changeRouteTo model  model.apiUrl (Route.fromUrl model.url)
            Err err -> (model, Cmd.none)
    ShowDialog name id -> ({ model | dialog = Just { name = name, id = id } }, Cmd.none)
    CancelDialog -> ({ model | dialog = Nothing }, Cmd.none)
    OkDialog id -> ({ model | dialog = Nothing }, deleteRecipe model.apiUrl id)
    NoAction -> (model, Cmd.none)

langAttribute : Lang -> Element.Attribute Msg
langAttribute lang =
    Element.htmlAttribute (Attributes.attribute "lang"
                               (case lang of
                                    EN -> "en"
                                    ES -> "es"
                               ))

view : Model -> Browser.Document Msg
view model =
    { title = t model.translations "appname"
    , body = [ Element.layout [ langAttribute model.language] (viewBody model) ]
    }

viewLang : Model -> Element Msg
viewLang model =
    row [ alignRight ]
        [ el [ padding 5 ] (Element.text (t model.translations "Language"))
        , Input.button [ padding 5 ]
            { onPress = Just (ChangeLang ES)
            , label = button (Element.text (t model.translations "languages.es"))
            }
        , Input.button [ padding 5 ]
            { onPress = Just (ChangeLang EN)
            , label = button (Element.text (t model.translations "languages.en"))
            }
        ]

focus : Element.Attribute Msg
focus = Element.focused [ Border.glow (rgb 0 0 1) 2 ]

viewHeader : Model -> Element Msg
viewHeader model =
    row [ width fill ]
        [ Element.link [ Region.heading 1, Font.size 30, focus ]
              { url = ""
              , label = Element.text (t model.translations "appname")
              }
        , viewLang model
        ]

viewBody : Model -> Element Msg
viewBody model =
    column [ padding 10, spacing 10, width fill ]
        [ viewHeader model
        , viewMain model
        ]

viewMain : Model -> Element Msg
viewMain model =
    el [ Region.mainContent, width fill ]
        (case model.status of
            Loading -> viewLoading model
            Loaded -> viewPage model
            LoadFailed s -> viewLoadFailed model s
        )

button : Element Msg -> Element Msg
button e = el [ Border.color (rgb 0 0 0), Border.width 2, Border.rounded 3, padding 5 ] e

deleteButton : Element Msg -> Element Msg
deleteButton e = el [ Border.color (rgb 1 0 0), Border.width 2, Border.rounded 3, padding 5 ] e

viewLoading : Model -> Element Msg
viewLoading model = Element.text (t model.translations "Loading")

viewLoadFailed : Model -> String -> Element Msg
viewLoadFailed model s = Element.text ((t model.translations "loadingError") ++ " " ++ s)

viewDialog : Model -> Dialog -> Element Msg
viewDialog model dialog =
    column []
        [ Element.text ((t model.translations "deleteConfirmation") ++ dialog.name)
        , row [ width fill ]
            [ Input.button [ alignLeft ]
                  { onPress = Just CancelDialog
                  , label = button (Element.text (t model.translations "Cancel"))
                  }
            , Input.button [ alignRight ]
                { onPress = Just (OkDialog dialog.id)
                , label = button (Element.text (t model.translations "Ok"))
                }
            ]
        ]

viewPage : Model -> Element Msg
viewPage model =
    case model.page of
        PNone -> Element.text (t model.translations "pageNotFound")
        PRecipes recipes ->
            case model.dialog of
                Nothing -> viewRecipes model recipes
                Just dialog -> viewDialog model dialog
        PRecipe recipe -> viewRecipe model recipe
        PNewRecipe -> viewNewRecipe model

viewRecipes : Model -> Recipes -> Element Msg
viewRecipes model recipes =
    column [ width fill, padding 10, spacing 10 ]
        [ button (Element.link [ focus ]
                      { url = "#/newrecipe/"
                      , label = Element.text (t model.translations "NewRecipe")
                      })
        , column [ width fill, spacing 10]
            (map (viewRecipe model) recipes)
        ]

viewRecipe : Model -> Recipe -> Element Msg
viewRecipe model recipe =
    column [ width fill ]
        [ row [ width fill ]
              [ Element.link [ Region.heading 2, Font.bold, focus ]
                    { url = "#/recipe/" ++ String.fromInt recipe.id
                    , label = Element.text recipe.name}
              , Input.button [ alignRight, focus ]
                  { onPress = Just (ShowDialog recipe.name recipe.id)
                  , label = deleteButton (Element.text (t model.translations "Delete"))
                  }
              ]
        , Element.text recipe.text
        ]

viewNewRecipe : Model -> Element Msg
viewNewRecipe model =
    column [ spacing 10 ]
        [ el [ Font.bold ] (Element.text (t model.translations "NewRecipe"))
        , Input.text []
            { text = model.name
            , label = Input.labelLeft [ width (px 100) ] (Element.text (t model.translations "Name"))
            , placeholder = Nothing
            , onChange = NameChanged
            }
        , Input.multiline []
            { text = model.text
            , label = Input.labelLeft [ width (px 100) ] (Element.text (t model.translations "Text"))
            , placeholder = Nothing
            , onChange = TextChanged
            , spellcheck = True
            }
        , row [ width fill ]
            [ Input.button [ alignLeft ]
                  { onPress = Just CancelNewRecipe
                  , label = button (Element.text (t model.translations "Cancel"))
                  }
            , Input.button [ alignRight ]
                { onPress = Just OkNewRecipe
                , label = button (Element.text (t model.translations "Ok"))
                }
            ]
        ]
