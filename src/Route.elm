module Route exposing (..)

import Url exposing (Url)
import Url.Parser as Parser exposing (Parser, (</>), int, map, oneOf, s, string)

type Route
    = Home
    | Recipe Int
    | NewRecipe

parser : Parser (Route -> a) a
parser =
  oneOf
    [ map Home Parser.top
    , map Recipe (s "recipe" </> int)
    , map NewRecipe (s "newrecipe")
    ]

fromUrl : Url -> Maybe Route
fromUrl url =
    { url | path = Maybe.withDefault "" url.fragment, fragment = Nothing }
        |> Parser.parse parser
